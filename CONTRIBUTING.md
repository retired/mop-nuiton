# Join the project

TODO

# Release


## Maven requirements

You need to get the following server registred in **settings.xml**

``` 
    <!--To deploy site (need a ssh key with publish user)-->
    <server>
      <id>nuiton-doc</id>
      <username>publish</username>      
      <filePermissions>664</filePermissions>
      <directoryPermissions>775</directoryPermissions>      
    </server>

    <!--To deploy in nuiton nexus (deprecated)-->
    <server>
      <id>nuiton-nexus</id>
      <username>deployment</username>
      <!--set nexus password-->
      <password>xxx</password>
    </server>
    
    <!--To deploy in ossrh nexus -->
        <server>
          <id>ossrh</id>
          <!--set user name-->
          <username>xxx</username>
          <!--set nexus password-->
          <password>xxx</password>
        </server>

    <!--To interact with gitlab-->
    <server>
      <id>nuiton-gitlab</id>
      <username>maven-release</username>
      <!--set api token-->
      <privateKey>xxx</privateKey>
    </server>

    <!--To sign-->
    <server>
      <id>nuiton-gpg</id>
      <username>Code Lutin</username>
      <!--set key password-->
      <password>xxx</password>
    </server>

    <!--To sign with keystore-->
    <server>
      <id>nuiton-keystore</id>
      <!-- path to key store -->
      <privateKey>xxx/.m2/CodeLutinKeystore</privateKey>
      <!-- password of the key store -->
      <password>Password of the key store
        {xxx}
      </password>
      <!-- name of the key alias -->
      <username>Alias of the key to use
        {xxx}
      </username>
      <!-- password of the key -->
      <passphrase>Password ot the key
        {xxx}
      </passphrase>
    </server>
```
