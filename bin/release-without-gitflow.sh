#!/usr/bin/env bash

# to perform a release using nuiton nexus and maven-release-plugin

PROJECT=$(grep -e "<projectId>" pom.xml | grep -v '${' | cut -d'>' -f2 | cut -d'<' -f1)

VERSION=$(grep -e "-SNAPSHOT" pom.xml | cut -d'>' -f2 | cut -d'<' -f1)
VERSION=${VERSION/-SNAPSHOT/}
LOG_DIR=/tmp/${PROJECT}-${VERSION}

echo "projectId: $PROJECT"
echo "version:   $VERSION"
echo "log dir:   $LOG_DIR"

rm -rf ${LOG_DIR}
mkdir -p ${LOG_DIR}

echo "Prepare release: $VERSION ($LOG_DIR/release-prepare.log)"
mvn release:prepare -B -Prelease-profile --log-file ${LOG_DIR}/release-prepare.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   exit 1
fi

echo "Perform release: $VERSION ($LOG_DIR/release-perform.log)"
mvn release:perform -Prelease-profile --log-file ${LOG_DIR}/release-perform.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   exit 1
fi