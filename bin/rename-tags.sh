#!/usr/bin/env bash

PROJECT=$(grep -e "<projectId>" pom.xml | grep -v '${' | cut -d'>' -f2 | cut -d'<' -f1)
echo "rename tags for $PROJECT"

prefix=$1

for i in $(git tag | grep -e "$prefix") ; do
  j=${i/${prefix}/v}
  echo "Rename $i to $j"
  git tag ${j} ${i}
  git tag -d ${i}
  git push origin :refs/tags/${i}
done
git push --tags
