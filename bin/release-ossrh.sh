#!/usr/bin/env bash

# to perform a release using ossrh nexus and jgitflow plugin

PROJECT=$(grep -e "<projectId>" pom.xml | grep -v '${' | cut -d'>' -f2 | cut -d'<' -f1)
MASTER=$(grep -e ".masterBranchName" pom.xml | grep -v "{" | cut -d'>' -f2 | cut -d'<' -f1)
if [ "$MASTER" == "" ]; then
  MASTER="master"
fi
DEVELOP=$(grep -e ".developBranchName" pom.xml | grep -v "{" | cut -d'>' -f2 | cut -d'<' -f1)
if [ "$DEVELOP" == "" ]; then
  DEVELOP="develop"
fi

VERSION=$(grep -e "-SNAPSHOT" pom.xml | cut -d'>' -f2 | cut -d'<' -f1)
VERSION=${VERSION/-SNAPSHOT/}
LOG_DIR=/tmp/${PROJECT}-${VERSION}

echo "projectId: $PROJECT"
echo "master:    $MASTER"
echo "develop:   $DEVELOP"
echo "version:   $VERSION"
echo "log dir:   $LOG_DIR"

rm -rf ${LOG_DIR}
mkdir -p ${LOG_DIR}

git checkout -B ${MASTER}
git checkout -B ${DEVELOP}

echo "Start release: $VERSION ($LOG_DIR/release-start.log)"
mvn jgitflow:release-start -B -Prelease-profile -Possrh --log-file ${LOG_DIR}/release-start.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   exit 1
fi
echo "Finish release: $VERSION ($LOG_DIR/release-finish.log)"
mvn jgitflow:release-finish -Prelease-profile -Possrh --log-file ${LOG_DIR}/release-finish.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   exit 1
fi