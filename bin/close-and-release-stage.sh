#!/usr/bin/env bash

# to close a stage using ossrh nexus

PROJECT=$(grep -e "<projectId>" pom.xml | grep -v '${' | cut -d'>' -f2 | cut -d'<' -f1)
STAGE=orgnuiton

VERSION=$(grep -e "-SNAPSHOT" pom.xml | cut -d'>' -f2 | cut -d'<' -f1)
VERSION=${VERSION/-SNAPSHOT/}
LOG_DIR=/tmp/${PROJECT}-${VERSION}

echo "projectId: $PROJECT"
echo "stage:     $STAGE"
echo "log dir:   $LOG_DIR"

echo "Get staging id..."
STAGE_ID=$(mvn nexus-staging:rc-list -N -Possrh | grep ${STAGE} | grep OPEN | cut -d' ' -f2)

echo "Closing stage: $STAGE_ID ($LOG_DIR/stage-close.log)"
mvn nexus-staging:close -N -Possrh -DstagingRepositoryId=${STAGE_ID} --log-file ${LOG_DIR}/stage-close.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   exit 1
fi

echo "Releasing stage: $STAGE_ID ($LOG_DIR/stage-release.log)"
mvn nexus-staging:release -N -Possrh -DstagingRepositoryId=${STAGE_ID} --log-file ${LOG_DIR}/stage-release.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   exit 1
fi